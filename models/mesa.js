module.exports = (sequelize, DataTypes) => {
  const Mesa = sequelize.define('mesa', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    nombre: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    id_restaurante: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    planta: {
      type: DataTypes.INTEGER,
      allowNull: false,
      default: 1
    },
    capacidad: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    coord_x: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    coord_y: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
  }, {
    tableName: 'mesas',
    timestamps:false
  });
  Mesa.associate = function (models) {
    models.mesa.belongsTo(models.restaurante, {
      foreignKey: 'id_restaurante',
      onDelete: 'NO ACTION',
      onUpdate: 'NO ACTION'
    });
  }
  return Mesa;
}
