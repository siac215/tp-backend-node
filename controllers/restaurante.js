const Restaurante = require('../models').restaurante;
/**
 * RestauranteController
 */
module.exports = {
  index: function(req, res, next) {
    return Restaurante.findAll()
    .then(
      (restaurantes) => res.status(200).send({'restaurantes': restaurantes})
    )
    .catch(
      (error) => res.status(400).send({'message': error.toString()})
    );
  },
  view: async function(req, res, next) {
    let restaurante = await Restaurante.findByPk(req.params.id);
    if (restaurante === null) {
      return res.status(404).send({'message': 'Restaurante no ha sido encontrado'});
    }
    return res.status(200).send(restaurante)
  },
  add: function(req, res, next) {
    return Restaurante.create({
      'nombre': req.body.nombre,
      'direccion': req.body.direccion
    })
    .then ((restaurante) => res.status(200).send(restaurante))
    .catch((error) => res.status(400).send(error));
  },
  edit: async function(req, res, next) {
    let restaurante = await Restaurante.findByPk(req.params.id);
    if (restaurante === null) {
      return res.status(404).send({'message': 'Restaurante no ha sido encontrado'});
    }
    restaurante.nombre = req.body.nombre;
    restaurante.direccion = req.body.direccion;
    return restaurante.save()
      .then((restaurante) => res.status(200).send(restaurante))
      .catch((error) => res.status(400).send({'message': 'No se ha podido guardar los cambios', 'error': error}));
  },
  delete: async function (req, res, next) {
    let restaurante = await Restaurante.findByPk(req.params.id);
    if (restaurante === null) {
      return res.status(404).send({'message': 'Restaurante no ha sido encontrado'});
    }
    return restaurante.destroy()
      .then((restaurante) => res.status(200).send(restaurante))
      .catch((error) => res.status(400).send({'message': 'No se ha podido eliminar', 'error': error}));
  }
}