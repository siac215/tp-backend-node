module.exports = (sequelize, DataTypes) => {
  const Reserva = sequelize.define('reserva', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    id_restaurante: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    id_mesa: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    fecha_inicio: {
      type: DataTypes.DATE,
      allowNull: false
    },
    fecha_fin: {
      type: DataTypes.DATE,
      allowNull: false
    },
    id_cliente: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    cantidad_solicitada: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    tableName: 'reservas',
    timestamps:false
  });
  Reserva.associate = function (models) {
    models.reserva.belongsTo(models.restaurante, {
      foreignKey: 'id_restaurante',
      onDelete: 'NO ACTION',
      onUpdate: 'NO ACTION'
    });
    models.reserva.belongsTo(models.mesa, {
      foreignKey: 'id_mesa',
      onDelete: 'NO ACTION',
      onUpdate: 'NO ACTION'
    });
    models.reserva.belongsTo(models.cliente, {
      foreignKey: 'id_cliente',
      onDelete: 'NO ACTION',
      onUpdate: 'NO ACTION'
    });
  }
  return Reserva;
}
