module.exports = (sequelize, DataTypes) => {
  const Restaurante = sequelize.define('restaurante', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    nombre: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    direccion: {
      type: DataTypes.STRING(200),
      allowNull: false
    }
  }, {
    tableName: 'restaurantes',
    timestamps:false
  });
  Restaurante.associate = function(models) {
    models.restaurante.hasMany(models.mesa, {
      foreignKey: 'id_restaurante'
    });
  }
  return Restaurante;
}