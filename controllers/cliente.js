const Cliente = require('../models').cliente;
/**
 * ClienteController
 */
module.exports = {
  index: function(req, res, next) {
    return Cliente.findAll()
    .then(
      (clientes) => res.status(200).send({'clientes': clientes})
    )
    .catch(
      (error) => res.status(400).send({'message': error.toString()})
    );
  },
  view: async function(req, res, next) {
    let cliente = await Cliente.findByPk(req.params.id);
    if (cliente === null) {
      return res.status(404).send({'message': 'Cliente no ha sido encontrado'});
    }
    return res.status(200).send(cliente)
  },
  add: function(req, res, next) {
    return Cliente.create({
      'cedula': req.body.cedula,
      'nombre': req.body.nombre,
      'apellido': req.body.apellido,
    })
    .then ((cliente) => res.status(200).send(cliente))
    .catch((error) => res.status(400).send(error));
  },
  edit: async function(req, res, next) {
    let cliente = await Cliente.findByPk(req.params.id);
    if (cliente === null) {
      return res.status(404).send({'message': 'Cliente no ha sido encontrado'});
    }
    cliente.cedula = req.body.cedula
    cliente.nombre = req.body.nombre;
    cliente.apellido = req.body.apellido;
    return cliente.save()
      .then((cliente) => res.status(200).send(cliente))
      .catch((error) => res.status(400).send({'message': 'No se ha podido guardar los cambios', 'error': error}));
  },
  delete: async function (req, res, next) {
    let cliente = await Cliente.findByPk(req.params.id);
    if (cliente === null) {
      return res.status(404).send({'message': 'Cliente no ha sido encontrado'});
    }
    return cliente.destroy()
      .then((cliente) => res.status(200).send({'message': 'Se ha eliminado'}))
      .catch((error) => res.status(400).send({'message': 'No se ha podido eliminar', 'error': error}));
  },
  cedula: async function(req, res, next) {
    let cliente = await Cliente.findOne({
      where: {
        'cedula': req.params.cedula
      }
    });
    if (cliente === null) {
      return res.status(404).send({'message': 'Cliente no ha sido encontrado'});
    }
    return res.status(200).send(cliente)
  },
}