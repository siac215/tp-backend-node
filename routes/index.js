var express = require('express');
var router = express.Router();
const {
  MesaController,
  RestauranteController,
  ClienteController,
  ReservaController
} = require('../controllers');

/* GET home page. */
/* router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
}); */

// Restaurantes
router.get('/api/restaurantes', RestauranteController.index);
router.get('/api/restaurantes/:id', RestauranteController.view);
router.post('/api/restaurantes', RestauranteController.add);
router.put('/api/restaurantes/:id', RestauranteController.edit);
router.delete('/api/restaurantes/:id', RestauranteController.delete);

// Clientes
router.get('/api/clientes', ClienteController.index);
router.get('/api/clientes/:id', ClienteController.view);
router.get('/api/clientes/cedula/:cedula', ClienteController.cedula);
router.post('/api/clientes', ClienteController.add);
router.put('/api/clientes/:id', ClienteController.edit);
router.delete('/api/clientes/:id', ClienteController.delete);

// Mesas
router.get('/api/mesas', MesaController.index);
router.get('/api/mesas/:id', MesaController.view);
router.post('/api/mesas', MesaController.add);
router.put('/api/mesas/:id', MesaController.edit);
router.delete('/api/mesas/:id', MesaController.delete);

// Reservas
router.get('/api/reservas/', ReservaController.index);
router.post('/api/reservas/reservar', ReservaController.reservar);
router.get('/api/reservas/disponibles', ReservaController.disponibles);
router.get('/api/reservas/buscar', ReservaController.buscar);

module.exports = router;
