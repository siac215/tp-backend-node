const Mesa = require('../models').mesa;
/**
 * MesaController
 */
module.exports = {
  index: function(req, res, next) {
    return Mesa.findAll()
    .then(
      (mesas) => res.status(200).send({'mesas': mesas})
    )
    .catch(
      (error) => res.status(400).send({'message': error})
    );
  },
  view: async function(req, res, next) {
    let mesa = await Mesa.findByPk(req.params.id);
    if (mesa === null) {
      return res.status(404).send({'message': 'Mesa no ha sido encontrado'});
    }
    return res.status(200).send(mesa)
  },
  add: function(req, res, next) {
    return Mesa.create({
      'nombre': req.body.nombre,
      'id_restaurante': req.body.id_restaurante,
      'planta': req.body.planta,
      'capacidad': req.body.capacidad,
      'coord_x': req.body.coord_x,
      'coord_y': req.body.coord_y
    })
    .then ((mesa) => res.status(200).send(mesa))
    .catch((error) => res.status(400).send(error));
  },
  edit: async function(req, res, next) {
    let mesa = await Mesa.findByPk(req.params.id);
    if (mesa === null) {
      return res.status(404).send({'message': 'Mesa no ha sido encontrado'});
    }
    mesa.nombre = req.body.nombre;
    mesa.id_restaurante = req.body.id_restaurante;
    mesa.planta = req.body.planta;
    mesa.capacidad = req.body.capacidad;
    mesa.coord_x = req.body.coord_x;
    mesa.coord_y = req.body.coord_y;
    return mesa.save()
      .then((mesa) => res.status(200).send(mesa))
      .catch((error) => res.status(400).send({'message': 'No se ha podido guardar los cambios', 'error': error}));
  },
  delete: async function (req, res, next) {
    let mesa = await Mesa.findByPk(req.params.id);
    if (mesa === null) {
      return res.status(404).send({'message': 'Mesa no ha sido encontrado'});
    }
    return mesa.destroy()
      .then((mesa) => res.status(200).send(mesa))
      .catch((error) => res.status(400).send({'message': 'No se ha podido eliminar', 'error': error}));
  }
}