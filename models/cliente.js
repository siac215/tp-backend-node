module.exports = (sequelize, DataTypes) => {
  const Cliente = sequelize.define('cliente', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    cedula: {
      type: DataTypes.STRING(15),
      allowNull: false,
      unique: true
    },
    nombre: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    apellido: {
      type: DataTypes.STRING(50),
      allowNull: false
    }
  }, {
    tableName: 'clientes',
    timestamps:false
  });
  return Cliente;
}