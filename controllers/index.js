const RestauranteController = require('./restaurante');
const ClienteController = require('./cliente');
const MesaController = require('./mesa');
const ReservaController = require('./reserva');
module.exports = {
  RestauranteController,
  ClienteController,
  MesaController,
  ReservaController
};