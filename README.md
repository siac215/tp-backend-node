# TP Backend Node

## Instalar

npm install

## Base de datos

Crear una base de datos con el nombre reservas e importar estructura y datos del archivo reservas.sql

## Ejecutar

npm run serve

## POSTMAN
Importar collection del archivo TP-Backend-Node.postman_collection.json