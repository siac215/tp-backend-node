const sequelize = require('../models').sequelize;
const Reserva = require('../models').reserva;
const Mesa = require('../models').mesa;
const Cliente = require('../models').cliente;
const Op = require('sequelize').Op;
const moment = require('moment');

/**
* ReservaController
*/
module.exports = {
  index: function(req, res, next) {
    return Reserva.findAll({
      include: Cliente
    })
    .then((reservas) => res.status(200).send({'reservas': reservas}))
    .catch((error) => res.status(400).send({'message': error}));
  },
  reservar: function(req, res, next) {
    Reserva.create({
      "id_restaurante": req.body.id_restaurante,
      "id_mesa": req.body.id_mesa,
      "fecha_inicio": moment(req.body.fecha_inicio).toDate(),
      "fecha_fin": moment(req.body.fecha_fin).toDate(),
      "id_cliente": req.body.id_cliente,
      "cantidad_solicitada": req.body.cantidad_solicitada
    })
    .then((reserva) => res.status(200).send(reserva))
    .catch((error) => res.status(400).send(error));
  },
  disponibles: function(req, res, next) {
    const fecha_inicio = moment(req.query.fecha_inicio).toDate();
    const fecha_fin = moment(req.query.fecha_fin).toDate();
    // Construir un query para usar como subquery
    const reservasQuery = sequelize.dialect.queryGenerator.selectQuery('reservas', {
      attributes: ['id_mesa'],
      where: {
        'id_restaurante': +req.query.id_restaurante,
        [Op.or]: {
          'fecha_inicio': {
            [Op.and]: {
              [Op.gte]: fecha_inicio,
              [Op.lte]: fecha_inicio
            }
          },
          'fecha_fin': {
            [Op.and]: {
              [Op.gte]: fecha_fin,
              [Op.lte]: fecha_fin
            }
          }
        }
      }
    })
    .slice(0,-1);
    return Mesa.findAll({
      where: {
        id_restaurante: +req.query.id_restaurante,
        id: {
          [Op.notIn]: sequelize.literal("(" + reservasQuery + ")")
        }
      }
    })
    .then((mesas) => res.status(200).send({'mesas': mesas}))
    .catch((error) => res.status(400).send({'message': error}));
  },
  buscar: function(req, res, next) {
    var where = {};
    if(typeof req.query.id_cliente === 'undefined') {
      where = {
        'id_restaurante': req.query.id_restaurante,
        [Op.and]: sequelize.where(sequelize.fn('date', sequelize.col('fecha_inicio')), '=', req.query.fecha)
      }
    } else {
      where = {
        'id_cliente': req.query.id_cliente,
        'id_restaurante': req.query.id_restaurante,
        [Op.and]: sequelize.where(sequelize.fn('date', sequelize.col('fecha_inicio')), '=', req.query.fecha)
      }
    }
    var query = Reserva.findAll({
      include: [Cliente, Mesa],
      where: where
    });

    return query.then((reservas) => res.status(200).send({'reservas': reservas}))
    .catch((error) => res.status(400).send({'message': error}));
  },
}